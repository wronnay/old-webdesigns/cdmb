# cdmb

![](screenshot-2014-01-24.png)

**License:** GCC BY-NC-SA 3.0 (https://creativecommons.org/licenses/by-nc-sa/3.0/)

## Background

cdmb.tk was an URL shortener based on YOURLS. This is the Frontend I created for it. This project is not online anymore and I decided to open source the Design.